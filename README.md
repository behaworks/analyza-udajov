# Analýza údajov

**Do repozitára nepatria jupyter-notebooky!**

Obsah notebooku treba skopírovať do obyčajného súboru `.py`, ktorý sa naformátuje podľa potreby.
Ostatné pythonové súbory treba naformátovať automaticky.
