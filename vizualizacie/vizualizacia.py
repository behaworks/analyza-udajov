#%%

import pandas as pd
import plotly.express as px
import plotly.graph_objs as go
import plotly.offline as po
from pymongo import MongoClient

#%%

mongo = MongoClient("mongodb://localhost:27017/")
db = mongo["behaworks_loggerv3"]
test_movement_collection = db["test_movement"]
test_button_collection = db["test_button"]

#%%

query = {"session_id": "67ba25a172144f2c"}
df = pd.DataFrame(list(test_movement_collection.find(query)))

#%%

fig = px.scatter_3d(df, x='x', y='y', z='z', animation_frame='timestamp')
fig.show()


#%%
splot = go.Scatter3d(
    x=df['x'],
    y=df['y'],
    z=df['z'],
    mode='markers',
    marker=dict(
        color='rgb(255, 0, 0)',
        size=1,
        symbol='circle',
        line=dict(
            color='rgb(255, 0, 0)',
            width=1
        )
    ),
    opacity=0.1
)
layout = go.Layout(
    width=1024,
    height=1024,
    scene = dict(
        camera=dict(up=dict(x=0, y=1, z=0)),
        xaxis=dict(title='X', range=[-3, 3]),
        yaxis=dict(title='Vyska', range=[-3, 3]),
        zaxis=dict(title='Z', range=[-3, 3])),
)

pdata = [splot]
fig = go.Figure(data=pdata, layout=layout)
fig.show()

# %%

splot_anim = [go.Scatter3d(
    x=df['x'],
    y=df['y'],
    z=df['z'],
    mode="lines",
    line=dict(width=2, color="blue"),
    opacity=0.1
), go.Scatter3d(
    x=df['x'],
    y=df['y'],
    z=df['z'],
    mode="lines",
    line=dict(width=2, color="blue"),
    opacity=0.1
)]
frames = [go.Frame(
    data=go.Scatter3d(
        x=df['x'][:k],
        y=df['y'][:k],
        z=df['z'][:k],
        mode='markers',
        marker=dict(
            color='rgb(255, 0, 0)',
            size=1,
            symbol='circle',
            line=dict(
                color='rgb(255, 0, 0)',
                width=1
            )
        ),

        opacity=0.1
    )
) for k in range(len(df['timestamp']))]

layout_anim = go.Layout(
    width=1024,
    height=1024,
    scene=dict(
        camera=dict(up=dict(x=0, y=1, z=0)),
        xaxis=dict(title='X', range=[-2, 2]),
        yaxis=dict(title='Vyska', range=[-2, 2]),
        zaxis=dict(title='Z', range=[-2, 2])),
    updatemenus=[dict(type="buttons",
                      buttons=[dict(label="Play",
                                    method="animate",
                                    args=[None])])]
)

fig_anim = go.Figure(data=splot_anim, layout=layout_anim, frames=frames)
frame_duration = df['timestamp'].tolist()[-1] / len(df['timestamp'])
po.plot(fig_anim, show_link=True, link_text='Graf', animation_opts=dict(frame=dict(duration=1)))
